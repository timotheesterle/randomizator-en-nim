import
  jester,
  strutils,
  tirage

routes:
  get "/":
    let nbGroupes
    = if request.params.contains("nbgroupes"):
      request.params["nbgroupes"].parseInt()
    else:
      3

    resp tirage.groupesAleatoires(nbGroupes)

runForever()
