import json, random, sequtils, moustachu

randomize()

proc groupesAleatoires*(nbDeGroupes = 3): string =
  let json = readFile("./apprenants.json").parseJson()
  var apprenants = json.mapIt(it.to(JsonNode))

  apprenants.shuffle()
  let groupes = apprenants.distribute(nbDeGroupes)

  var jsonGroupes: seq[JsonNode] = @[]
  for index, groupe in groupes:
    jsonGroupes.add(%* { "groupe": groupe, "index": index + 1 })

  let fields = newContext(
    %* {
      "pageTitle": "Randomizator, en Nim !",
      "groupes": jsonGroupes,
    }
  )

  let view = readFile("./views/index.mustache")
  return render(view, fields)
