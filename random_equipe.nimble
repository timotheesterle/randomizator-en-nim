# Package

version       = "0.1.0"
author        = "Timothée Sterle"
description   = "A new awesome nimble package"
license       = "MIT"
srcDir        = "."
bin           = @["server"]



# Dependencies

requires "nim >= 1.2.0"
requires "jester"
requires "moustachu"
